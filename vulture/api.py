import requests
import json

import sys
import os
import traceback

from csv import writer
import jsonschema
from jsonschema import validate

reslist = []

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class steller_api:
    def get_bearer_token():
        login_url = "https://qa-finops.aquilaclouds.com" + "/api/auth/login"
#        login_payload = "{\"email\":\"kumareswar.kandimalla@sifycorp.com\",\"password\":\"sify#Adm1n\"}"
        login_payload = "{\"email\":\"testba@ac.com\",\"password\":\"Sify#Adm1n\"}"
        login_headers = {
            'Content-Type': 'application/json'
        }

        login_response = requests.request("POST", login_url, headers=login_headers, data = login_payload)
        if login_response == None or login_response.status_code != 200:
            print("No response from server")
            return -1
        else:
            x = json.loads(login_response.text)
            return x['authToken']
    
    def get_id(self):
        return self.id_

def aquila_assert(a, b):
    if a == b:
        reslist.append(b)
        return True
    else:
        reslist.append("Mismatch")
        return False

def aquila_assert_not(a, b):
    if a == b:
        reslist.append("Match with Data Failed")
        return False
    else:
        reslist.append(a)
        return True

def aquila_validate_response_with_data(data_file, response_text):
    with open(data_file, 'w') as df:
        df.write(response_text)
    
    with open(data_file, 'r') as df:
        if df.read() == response_text:
            return True
        else:
            return False


def write_to_csv(filename):
    with open(filename, 'a') as file:
        write_o = writer(file)
        write_o.writerow(reslist)
        file.close()

def assert_val(t="Z"):
    payload = adjustment_payload_slabbed_discount()
    if (t == "slabRate"):
        x = payload.find("\"slabRate\" :\"0\",\"rate\":\"0\"")
        if (x == -1):
            exit(x)
        x = payload.find("\"slabRate\" :\"500\",\"rate\":\"5\"")
        if (x == -1):
            exit(x)
        x = payload.find("\"slabRate\" :\"1000\",\"rate\":\"10\"")
        if (x == -1):
            exit(x)

def validate_json(json_data):
    try:
        validate(instance=json_data, schema=student_schema)
    except:
        return False
    return True


OPERATION_ADD = "OPERATION_ADD"
OPERATION_DELETE = "OPERATION_DELETE"
OPERATION_EDIT = "OPERATION_EDIT"
UNIMPLEMENTED = "API_NOT_IMPLEMENTED"


# Useful Governance APIs
API_STELLER_AUTOCREATE = "https://qa-finops.aquilaclouds.com/api/finops/domain/autocreate"
API_STELLER_STACKBAR = "https://qa-finops.aquilaclouds.com/api/finops/domains/governance/stackbar"
API_STELLER_FETCH = "https://qa-finops.aquilaclouds.com/api/finops/domains/summary/fetch"
API_STELLER_ADDDOMAIN = "https://qa-finops.aquilaclouds.com/api/finops/domain/save?type=add"
API_STELLER_DEL = "https://qa-finops.aquilaclouds.com/api/finops/domain/delete"
API_STELLER_ADDSERVICEDOMAIN = "https://qa-finops.aquilaclouds.com/api/finops/servdomain/save?type=add"
API_STELLER_DELSERVICEDOMAIN = UNIMPLEMENTED
API_STELLER_EDITSERVICEDOMAIN = UNIMPLEMENTED
API_STELLER_EDITACCOUNT = UNIMPLEMENTED
API_STELLER_EDITTAG = UNIMPLEMENTED
'''
A varient for save which returns OPERATION_EDIT. 
'''
API_STELLER_EDITDOMAIN = "https://qa-finops.aquilaclouds.com/api/finops/domain/save?type=edit"
'''
API finops explorer/overview summary corrosponds tp the summary view for explorer costs
'''
API_STELLER_SUMMARY = "https://qa-finops.aquilaclouds.com/api/finops/domains/explorer/overview/summary"
'''
API governance/overview/summary corrosponds to the summary view on the domains details page for governance
'''
API_STELLER_EXTENDED_SUMMARY = "https://qa-finops.aquilaclouds.com/api/finops/domains/governance/extended/summarypie"

'''
API heirchy corrosponds to the tree of all domains. Useful for listing newly created domains and their domain id 
'''
API_STELLER_HIER = "https://qa-finops.aquilaclouds.com/api/finops/domains/hierarchy"

# Useful explorer APIs
API_STELLER_EXPLORER_SUMMARY = "https://qa-finops.aquilaclouds.com/api/finops/domains/explorer/overview/summary"
API_STELLER_EXPLORER_FETCH_SUMMARY_DETAILED = "https://qa-finops.aquilaclouds.com/api/finops/domains/summary/fetch"
API_STELLER_RESOURCE_TYPE = "https://qa-finops.aquilaclouds.com/api/finops/domains/summary/resourcetype"
API_STELLER_TIMEFILTERS = "https://qa-finops.aquilaclouds.com/api/finops/timefilters"
API_STELLER_FETCH_INSTANCE = "https://qa-finops.aquilaclouds.com/api/finops/resources/cost/summary/fetch?type=instance"
API_STELLER_FETCH_STORAGE = "https://qa-finops.aquilaclouds.com/api/finops/resources/cost/summary/fetch?type=storage"
API_STELLER_SCALING_GROUP_FETCH = "https://qa-finops.aquilaclouds.com/api/finops/scalinggroup/cost/summary/fetch"
API_STELLER_TIMESERIES_COST_SG = "https://qa-finops.aquilaclouds.com/api/finops/scalinggroup/cost/timeseries"
API_STELLER_TIMESERIES_COUNT_SG = "https://qa-finops.aquilaclouds.com/api/finops/scalinggroup/count/timeseries"
API_STELLER_SERVICES_FETCH = "https://qa-finops.aquilaclouds.com/api/finops/servdomains/summary/fetch"
API_STELLER_RESERVATION_SUMMARY = "https://qa-finops.aquilaclouds.com/api/finops/explorer/reservation/summary"
API_STELLER_SUMMARY_LIST_RESERVATION = "https://qa-finops.aquilaclouds.com/api/finops/explorer/reservation/summary/list"

# Governance Payloads
PAYLOAD_AUTOCREATE_AWS="{\"userId\":125882,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"environment\",\"list\":[\"AWS\"],\"orignalData\":false}],\"drillParams\":{}}"
PAYLOAD_AUTOCREATE_AZURE="{\"userId\":125882,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"environment\",\"list\":[\"Azure\"],\"orignalData\":false}],\"drillParams\":{}}"
PAYLOAD_AUTOCREATE_GCP="{\"userId\":125882,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"environment\",\"list\":[\"GCP\"],\"orignalData\":false}],\"drillParams\":{}}"
PAYLOAD_STACKBAR = "{\"filters\":{},\"drillParams\":{\"hierarchyKey\":\"groupId\"},\"userId\":125882,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_FETCH = "{\"filters\":{},\"drillParams\":{\"groupId\":\"280\"},\"userId\":125882,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_FETCH_DETAILED = "{\"filters\":{},\"drillParams\":{\"groupId\":\"27\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_ADDDOMAIN_ZED_BUDGET_1000 = "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"Zed\",\"CREATE_BUDGET\":\"yes\",\"BUDGET\":\"1000\",\"RESET_PERIOD\":\"YEARLY\",\"CURRENCY\":\"\",\"START_DATE\":1616056143431,\"END_DATE\":1647592150682}},{\"key\":\"accountsSelection\"},{\"key\":\"tagsSelection\"}]}"
PAYLOAD_ADDDOMAIN_TAGGED_WPS = "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"ZED_TAG4\",\"CREATE_BUDGET\":\"no\",\"BUDGET\":\"\",\"RESET_PERIOD\":\"\",\"CURRENCY\":\"\",\"START_DATE\":1616060829439,\"END_DATE\":1618739233786}},{\"key\":\"accountsSelection\"},{\"key\":\"tagsSelection\",\"values\":[{\"label\":\"Windows Private Server\",\"key\":\"Windows Private Server\",\"type\":\"Windows Private Server\",\"_depth\":0,\"_id\":\"rdts2-0\",\"_children\":[],\"checked\":true,\"_focused\":true}]}]}"
PAYLOAD_DELETE_AQUILA_MARKETING = "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"groupId\",\"value\":235}]}"
PAYLOAD_DELETE_ZED = "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"groupId\",\"value\":489}]}"
PAYLOAD_FETCH_DELETE = "{\"filters\":{},\"drillParams\":{\"groupId\":\"304\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_ADD_RESOURCES_ACCOUNTS_DOMAIN = "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"testdom4\",\"CREATE_BUDGET\":\"yes\",\"BUDGET\":\"1000\",\"RESET_PERIOD\":\"MONTHLY\",\"CURRENCY\":\"\",\"START_DATE\":1616402079592,\"END_DATE\":1619080482489}},{\"key\":\"accountsSelection\",\"values\":[{\"label\":\"SifyAzureStack\",\"key\":\"CloudEnvironment [environmentId=150780, environmentName=SifyAzureStack, environmentType=AZURE_STACK]\",\"type\":\"cluster\",\"_depth\":0,\"_id\":\"rdts2-0\",\"_children\":[\"rdts2-0-0\",\"rdts2-0-1\",\"rdts2-0-2\",\"rdts2-0-3\",\"rdts2-0-4\",\"rdts2-0-5\",\"rdts2-0-6\",\"rdts2-0-7\",\"rdts2-0-8\",\"rdts2-0-9\",\"rdts2-0-10\",\"rdts2-0-11\",\"rdts2-0-12\"],\"_focused\":false,\"checked\":true,\"expanded\":false},{\"label\":\"AQ-Oracle\",\"key\":\"CloudEnvironment [environmentId=401944, environmentName=AQ-Oracle, environmentType=OCI]\",\"type\":\"cluster\",\"_depth\":0,\"_id\":\"rdts2-1\",\"_children\":[\"rdts2-1-0\"],\"checked\":true,\"_focused\":true,\"expanded\":false}]},{\"key\":\"tagsSelection\"}]}"
PAYLOAD_ADD_RESOURCES_ACCOUNTS_DOMAIN2 ="{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"testdom4\",\"CREATE_BUDGET\":\"yes\",\"BUDGET\":\"1000\",\"RESET_PERIOD\":\"MONTHLY\",\"CURRENCY\":\"\",\"START_DATE\":1616402079592,\"END_DATE\":1619080482489}},{\"key\":\"accountsSelection\",\"values\":[{\"label\":\"SifyAzureStack\",\"key\":\"CloudEnvironment [environmentId=150780, environmentName=SifyAzureStack, environmentType=AZURE_STACK]\",\"type\":\"cluster\",\"_depth\":0,\"_id\":\"rdts2-0\",\"_children\":[\"rdts2-0-0\",\"rdts2-0-1\",\"rdts2-0-2\",\"rdts2-0-3\",\"rdts2-0-4\",\"rdts2-0-5\",\"rdts2-0-6\",\"rdts2-0-7\",\"rdts2-0-8\",\"rdts2-0-9\",\"rdts2-0-10\",\"rdts2-0-11\",\"rdts2-0-12\"],\"_focused\":false,\"checked\":true,\"expanded\":false},{\"label\":\"AQ-Oracle\",\"key\":\"CloudEnvironment [environmentId=401944, environmentName=AQ-Oracle, environmentType=OCI]\",\"type\":\"cluster\",\"_depth\":0,\"_id\":\"rdts2-1\",\"_children\":[\"rdts2-1-0\"],\"checked\":true,\"_focused\":true,\"expanded\":false}]},{\"key\":\"tagsSelection\"}]}"
PAYLOAD_ADD_RESOURCES_TAGS_DOMAIN ="{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"testzeno\",\"CREATE_BUDGET\":\"yes\",\"BUDGET\":\"1000\",\"RESET_PERIOD\":\"MONTHLY\",\"CURRENCY\":\"\",\"START_DATE\":1616403573776,\"END_DATE\":1619168375446}},{\"key\":\"accountsSelection\"},{\"key\":\"tagsSelection\",\"values\":[{\"label\":\"j-2LHJH7IRFAO63\",\"key\":\"j-2LHJH7IRFAO63\",\"type\":\"j-2LHJH7IRFAO63\",\"children\":[],\"checked\":true},{\"label\":\"j-659YBJGURQW8\",\"key\":\"j-659YBJGURQW8\",\"type\":\"j-659YBJGURQW8\",\"children\":[],\"checked\":true},{\"label\":\"UAT-Matrix-MicroService-ALB\",\"key\":\"UAT-Matrix-MicroService-ALB\",\"type\":\"UAT-Matrix-MicroService-ALB\",\"children\":[],\"checked\":true},{\"label\":\"arn:aws:cloudformation:ap-south-1:901904061701:stack/awseb-e-gq5t32mbu5-stack/04892880-7154-11e9-8c75-02bc7d16c3bc\",\"key\":\"arn:aws:cloudformation:ap-south-1:901904061701:stack/awseb-e-gq5t32mbu5-stack/04892880-7154-11e9-8c75-02bc7d16c3bc\",\"type\":\"arn:aws:cloudformation:ap-south-1:901904061701:stack/awseb-e-gq5t32mbu5-stack/04892880-7154-11e9-8c75-02bc7d16c3bc\",\"children\":[],\"checked\":true},{\"label\":\"main-tally-mig\",\"key\":\"main-tally-mig\",\"type\":\"main-tally-mig\",\"children\":[],\"checked\":true}]}]}\r\n"
PAYLOAD_EDITDOMAIN = "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"294\",\"groupName\":\"Aquila Clouds Marketing Edit\",\"CREATE_BUDGET\":\"no\",\"BUDGET\":\"1000\",\"RESET_PERIOD\":\"MONTHLY\",\"CURRENCY\":\"USD\",\"START_DATE\":\"2020-09-01T00:00:00.000+00:00\",\"END_DATE\":\"2121-03-01T00:00:00.000+00:00\"}},{\"key\":\"accountsSelection\",\"values\":[]},{\"key\":\"tagsSelection\",\"values\":[]}]}\r\n"
PAYLOAD_SUMMARY = "{\"filters\":{},\"drillParams\":{\"groupId\":\"304\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_FETCH_CONSUMPTIONS = "{\"filters\":{},\"drillParams\":{\"groupId\":\"\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_FETCH_DETAILED2 = "{\"filters\":{},\"drillParams\":{\"groupId\":\"230\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_FETCH_BUDGET_DETAILED = "{\"filters\":{},\"drillParams\":{\"groupId\":\"304\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_FETCH_BUDGET = "{\"filters\":{},\"drillParams\":{\"groupId\":\"\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_SUMMARY_DETAILED = "{\"filters\":{},\"drillParams\":{\"groupId\":\"304\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_EXTENDED_SUMMARY_DETAILED = "{\"filters\":{},\"drillParams\":{\"groupId\":\"304\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_HIER_VIEW = "{\"filters\":{},\"drillParams\":{\"hierarchyKey\":\"groupId\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114]}"
PAYLOAD_ADD_UNBUDGETED = "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"testdom882\",\"CREATE_BUDGET\":\"no\",\"BUDGET\":\"\",\"RESET_PERIOD\":\"MONTHLY\",\"CURRENCY\":\"\",\"START_DATE\":1616737722386,\"END_DATE\":1619416125503}},{\"key\":\"accountsSelection\"},{\"key\":\"tagsSelection\"}]}"
PAYLOAD_EDIT_SIFYCSV = "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\"},{\"key\":\"accountsSelection\",\"values\":[]},{\"key\":\"tagsSelection\",\"values\":[]}]}"
PAYLOAD_AUTOCREATE_GARBAGE="{\"userId\":125882,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"environment\",\"list\":[\"GARBAGE\"],\"orignalData\":false}],\"drillParams\":{}}"
PAYLOAD_ADD_SERVICE_DOMAIN = "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\",\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"DemonstrationByZed\"}},{\"key\":\"resourcesSelection\",\"values\":[]}]}"
PAYLOAD_ADD_DOMAIN_RESOURCES = "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\",\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"testDom1\",\"CREATE_BUDGET\":\"no\",\"BUDGET\":\"\",\"RESET_PERIOD\":\"\",\"CURRENCY\":\"\",\"TIME_RANGE\":\"1616265000_1616869799\"}},{\"key\":\"accountsSelection\"},{\"key\":\"tagsSelection\"},{\"key\":\"cldsevicesSelection\"},{\"key\":\"containersSelection\"},{\"key\":\"resourcesSelection\",\"values\":[]},{\"key\":\"resGrpSelection\"}]}"
PAYLOAD_EXPLORER_FETCH = "{\"filters\":{},\"drillParams\":{\"groupId\":\"\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_TIMEFILTER = "{\"drillParams\":{},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_FETCH_INSTANCE = "{\"filters\":{},\"drillParams\":{\"groupId\":\"\",\"providerType\":\"\",\"topTimeFilter\":\"\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_TIMESERIES_INSTANCE_COUNT_SG = "{\"filters\":{},\"drillParams\":{\"instanceId\":\"arn:aws:autoscaling:us-east-2:244971728197:autoScalingGroup:e51fd8b8-ebe7-43a2-8661-92e1cce02178:autoScalingGroupName/asg-1\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_TIMESERIES_UTILIZATION_COUNT_SG = "{\"filters\":{},\"drillParams\":{\"instanceId\":\"arn:aws:autoscaling:us-east-2:244971728197:autoScalingGroup:e51fd8b8-ebe7-43a2-8661-92e1cce02178:autoScalingGroupName/asg-1\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_SERVICES_FETCH = "{\"filters\":{},\"drillParams\":{\"groupId\":\"\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"

def payload_delete_group_id(id):
    return "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"groupId\",\"value\":" + str(id) + "}]}"

def payload_add_unbudgeted(name):
    return "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"" + str(name) + "\",\"CREATE_BUDGET\":\"no\",\"BUDGET\":\"\",\"RESET_PERIOD\":\"MONTHLY\",\"CURRENCY\":\"\",\"START_DATE\":1616737722386,\"END_DATE\":1619416125503}},{\"key\":\"accountsSelection\"},{\"key\":\"tagsSelection\"}]}"

def payload_add_domain(name, budget):
    return "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"" + str(name) + "\",\"CREATE_BUDGET\":\"yes\",\"BUDGET\":\"" + str(budget) + "\",\"RESET_PERIOD\":\"YEARLY\",\"CURRENCY\":\"\",\"START_DATE\":1616056143431,\"END_DATE\":1647592150682}},{\"key\":\"accountsSelection\"},{\"key\":\"tagsSelection\"}]}"
 
def payload_add_domain_with_gid(gid, name, budget):
    return "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"" + str(gid) + "\",\"groupName\":\"" + str(name) + "\",\"CREATE_BUDGET\":\"yes\",\"BUDGET\":\"" + str(budget) + "\",\"RESET_PERIOD\":\"YEARLY\",\"CURRENCY\":\"\",\"START_DATE\":1616056143431,\"END_DATE\":1647592150682}},{\"key\":\"accountsSelection\"},{\"key\":\"tagsSelection\"}]}"

def payload_platform_add_domain_with_gid(gid, name, budget, platform):
    return "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"" + str(gid) + "\",\"groupName\":\"" + str(name) + "\",\"CREATE_BUDGET\":\"yes\",\"BUDGET\":\"" + str(budget) + "\",\"RESET_PERIOD\":\"YEARLY\",\"CURRENCY\":\"\",\"START_DATE\":1616056143431,\"END_DATE\":1647592150682}},{\"key\":\"accountsSelection\"},{\"key\":\"tagsSelection\"}]}"


def payload_add_domain_resource_unbudgeted(name):
    return "{\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\",\"params\":[{\"key\":\"formNewDomain\",\"formData\":{\"groupId\":\"\",\"groupName\":\"" + str(name) + "\",\"CREATE_BUDGET\":\"no\",\"BUDGET\":\"\",\"RESET_PERIOD\":\"\",\"CURRENCY\":\"\",\"TIME_RANGE\":\"1616265000_1616869799\"}},{\"key\":\"accountsSelection\"},{\"key\":\"tagsSelection\"},{\"key\":\"cldsevicesSelection\"},{\"key\":\"containersSelection\"},{\"key\":\"resourcesSelection\",\"values\":[]},{\"key\":\"resGrpSelection\"}]}"

# Explorer Payloads
PAYLOAD_EXPLORER_SUMMARY_MAIN = "{\"filters\":{},\"drillParams\":{\"groupId\":\"\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_EXPLORER_FETCH_DETAILED = "{\"filters\":{},\"drillParams\":{\"groupId\":\"304\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_FETCH_EXPLORER = "{\"filters\":{},\"drillParams\":{\"groupId\":\"304\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_EXPLORER_RESOURCE_TYPE = "{\"drillParams\":{\"groupId\":304},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_EXPLORER_FETCH_STORAGE = "{\"filters\":{},\"drillParams\":{\"groupId\":\"\",\"providerType\":\"\",\"topTimeFilter\":\"\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_SCALING_GROUP_FETCH = "{\"filters\":{},\"drillParams\":{},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_RESERVATION_SUMMARY = "{\"filters\":{},\"drillParams\":{\"topTimeFilter\":\"Y_0\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"
PAYLOAD_RESERVATION_FETCH_LIST = "{\"filters\":{},\"drillParams\":{\"hierarchyKey\":\"groupId\",\"topTimeFilter\":\"M_0\"},\"userId\":379946,\"envList\":[401954,133780,6,353193,4140977,426040,150780,112719,401944,369114],\"serviceKey\":\"finops\"}"

def VALIDATE_API_IMPLEMENTATION(url):
    try:
        assert(url != UNIMPLEMENTED)
    except AssertionError:
        _, _, tb = sys.exc_info()
        traceback.print_tb(tb)
        tb_info = traceback.extract_tb(tb)
        filename, line, func, text = tb_info[-1]
        print("The API is not yet implemeted in this drop. The error occured on line {} in statement {}".format(line, text)) 
        reslist.append("The API is not yet implemeted in this drop. The error occured on line {} in statement {}".format(line, text))
        write_to_csv('res.csv')
        exit(1)


def DESCRIBE_TEST_NAME(str):
    reslist.append(str)

def DESCRIBE_TEST_SPEC(str):
    reslist.append(str)

def DESCRIBE_TEST_URL(str):
    reslist.append(str)
    return str

def aws_validate():
    pass
