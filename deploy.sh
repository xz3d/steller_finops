echo "a2i deployment"

A2I_FILE_GZ=/home/ubuntu/steller_builds/steller.tar.gz

if test -f "$FILE"; then
    echo "$FILE exists [*]"
    sudo tar xvzf "$FILE" /data/temp/repository/views

    echo "killing existing java processes but not jenkins"
    sudo pkill -9 `ps -eaf | grep 'java' | grep -v 'jenkins' | awk '{print $2}'`

    echo "taking backup"
    sudo rm -rf /data/aquila/repository/views_bkup/*

    echo "moving the current jars from views to backup"
    sudo mv /data/aquila/repository/views/* /data/aquila/repository/views_bkup/
    
    echo "moving the new jars to install locations"
    sudo mv /data/temp/repository/views/* /data/aquila/repository/views/

    sudo mv /data/aquila/repository/lib/*.jar /data/aquila/repository/lib/backup
    sudo mv /data/temp/*.jar /data/aquila/repository/lib/

    echo "restarting services"
    sudo sh /data/aquila/repository/lib/startall.sh
else
    echo "$FILE does not exists [-]"
fi
