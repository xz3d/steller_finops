import yagmail

receiver = "sankrant@gmail.com"
body = '''
-------------- DEV BUILD PIPELINE ---------------------

AUTOMATED TEST RUN DEV PIPELINE

*FAIL* due to external errors

-------------------------------------------------------

'''
filename = "res.csv"

yag = yagmail.SMTP("smtp.gmail.com")
yag.send(
    to=receiver,
    subject="[AUTO] DEV PIPELINE",
    contents=body, 
    attachments=filename,
)
