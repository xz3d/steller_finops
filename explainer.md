 Domain Data Representation
----------------------------

TOP_LEVEL_DOMAIN (Company)                     |
 	StichedDomain1				               |
 		StichedSubDomain1                      |
		StichedSubDomain2                      |
		StichedSubDomain3                      |  
		StichedSubDomain4                      |
		..				                       |
	StichedDomain2				               |
 	        StichedSubDomain5                  |
		..                                     |
		..                                     |
	StichedDomain3                             |
 		..                                     |
		..                                     |
	StichedDomain4                             |
	..                                         |
	..                                         |
	..                                         |
	..
	DemonstrationByZed	..

...