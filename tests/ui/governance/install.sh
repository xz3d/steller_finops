#!/usr/bin/sh

# Set up chrome
echo "Getting latest chrome.."
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome-stable_current_amd64.deb

# Set up java
echo "Get Java.."
sudo apt install default-jdk

# Set up jenkins
# Creates a user called jenkins
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt update
sudo apt install jenkins

# start the service
sudo systemctl start jenkins

# activate the first install from the web dashboard by copying the key from /var/lib/jenkins/key
# create a new user
# generate a token from user management
# save the token

# Set up python deps
sudo apt install python3-pip
pip3 install selenium
pip3 install requests json

# The project ${WORKSPACE} goes into /var/lib/jenkins/workspace
# Go there and clone the repo 
git clone https://bitbucket.org/xz3d/steller_finops.git

# Now make the driver executable at drivers/linux/chromedriver
sudo chmod a+x drivers/linux/chromedriver

# change the owner of res.csv and groupid.k
sudo chown -R jenkins:jenkins res.csv
sudo chown -R jenkins:jenkins groupid.k

# Get jenkins-cli jar from
http://JENKINS_DEP/jnlpJars/jenkins-cli.jar

# The jenkins cli can be used in the following way
java -jar .\jenkins-cli.jar -s http://JENKINS_DEP/ -auth <user>:<TOKEN> command