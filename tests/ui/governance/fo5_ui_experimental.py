import selenium
import sys
import os
import time
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/home/ubuntu/test_pipeline/steller_finops")

import os

try:
  os.chdir("/var/lib/jenkins/workspace/Test/steller_finops/")
  print("Directory changed")
except OSError:
  print("Can't change the Current Working Directory")  

from vulture.ui import *

def main():
    base = Base(sys.argv[1], sys.argv[2], sys.argv[3])
    driver = base.getData()["driver"]

    # Code goes inside this try block
    # DO NOT CODE OUTISDE UNLESS YOU ARE SURE THERE WILL BE NO ERRORS
    # try:
    base.login("testba@ac.com", "Sify#Adm1n")
    base.openFinOpsService()
    base.navigateToMenu(3, "#/governance/domain")

    # Required here since the page takes some time to load
    time.sleep(10)

    add_button = base.getElementByXpath('/html/body/div[1]/div/div[3]/div[3]/div[1]/div[2]/div/button[1]').click()

    time.sleep(5)

    group_name_text_field = base.getElementByXpath('/html/body/div/div/div[3]/div[3]/div[3]/div[1]/div/div/div[3]/div/div/div/div[1]/div[2]/div/div/div/input').send_keys("DemonstrationByZed")

    time.sleep(1)

    time_period = base.getElementByXpath('/html/body/div[1]/div/div[3]/div[3]/div[3]/div[1]/div/div/div[3]/div/div/div/div[6]/div/div/span[1]/input').send_keys("Monthly\r")
    select_time_period = base.getElementByXpath('/html/body/div[2]/div/div/div/div[2]/div[1]/div/div/div[1]/div').click()

    start_date = base.getElementByXpath('/html/body/div[1]/div/div[3]/div[3]/div[3]/div[1]/div/div/div[3]/div/div/div/div[8]/div[2]/div/div/div/input').send_keys("03/25/2021")
    end_date = base.getElementByXpath('/html/body/div[1]/div/div[3]/div[3]/div[3]/div[1]/div/div/div[3]/div/div/div/div[9]/div[2]/div/div/div/input').send_keys("04/25/2021\n")
    time.sleep(1)

    """    
    add_account_button = base.getElementByXpath('/html/body/div[1]/div/div[3]/div[3]/div[3]/div[3]/div/div[2]/div/div[1]/div/div[1]/div[2]/button[2]').click()

    time.sleep(4)

    input_check1 = base.getElementByXpath('/html/body/div[5]/div[3]/div/div[2]/div/div/div/div[2]/input').click()

    action = ActionChains(driver)
    apply_button = base.getElementByXpath('/html/body/div[5]/div[3]/div/div[3]/button').click()
    action.double_click(apply_button).perform()

    time.sleep(4)
    """
    save_ui = base.getElementByXpath('/html/body/div[1]/div/div[3]/div[3]/div[4]/div/div/button').click()
    time.sleep(2)
    apply_final = base.getElementByXpath('/html/body/div[5]/div[3]/div/div[2]/div[3]/button[2]').click()
    # except:
    #     base.setResult(False)
    #     print("Test failed due to an unexpected error")

    driver.quit()
    return base.getData()["result"]


if __name__ == '__main__':
    testCaseName = os.path.basename(__file__).split('.', 1)[0]
    print("Running " + testCaseName)
    start_time = time.time()
    result = main()
    bearer = steller_api.get_bearer_token() 
    headers = {
        'Authorization': 'Bearer ' + str(bearer),
        'Content-Type': 'application/json'
    }
    url = API_STELLER_HIER
    response = requests.request("POST", url, headers=headers, data=PAYLOAD_HIER_VIEW)
    responsejson = json.loads(response.text)

    print(len(responsejson[0]))

    for i in responsejson[0]['children']:
      if i['groupName'] == "DemonstrationByZed":
        f = open('groupid.k', 'w+')
        if (i['groupId'] == i['key']):
          f.write(str(i['key']))
          f.close()
        result = True
        break
      else:
        result = False

    if result:
        print(testCaseName + " passed")
    else:
        print(testCaseName + " failed")
    print("Finished in: %s seconds" % (time.time() - start_time))
