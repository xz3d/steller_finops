import selenium
import sys
import os
from vulture.ui import *
from vulture.api import *
import time

def main():
    base = Base(sys.argv[1], sys.argv[2], sys.argv[3])
    driver = base.getData()["driver"]

    # Code goes inside this try block
    # DO NOT CODE OUTISDE UNLESS YOU ARE SURE THERE WILL BE NO ERRORS
    # try:
    base.login("testba@ac.com", "Sify#Adm1n")
    base.openFinOpsService()
    base.navigateToMenu(3, "#/governance/domain")

    # Required here since the page takes some time to load
    time.sleep(10)

    X = base.getElementByXpath('/html/body/div[1]/div/div[3]/div[3]/div[3]/div[1]/div/div[1]/div[2]/div[2]').text
    
    time.sleep(2)

    detail_button = base.getElementByXpath('/html/body/div[1]/div/div[3]/div[3]/div[3]/div[3]/div/div[3]/table/tbody/tr/td[8]/div[2]/div/div/button').click()

    time.sleep(5)

    Y = base.getElementByXpath('/html/body/div[1]/div/div[3]/div[3]/div[3]/div[1]/div/div[1]/div[2]/div[2]').text

    # except:
    #     base.setResult(False)
    #     print("Test failed due to an unexpected error")

    driver.quit()
    #return base.getData()["result"]
    return (X == Y)


if __name__ == '__main__':
    testCaseName = os.path.basename(__file__).split('.', 1)[0]
    print("Running " + testCaseName)
    start_time = time.time()
    result = main()
    if result:
        reslist.append(testCaseName)
        reslist.append("PASS")
        print(testCaseName + " passed")
    else:
        reslist.append(testCaseName)
        reslist.append("FAIL")
        print(testCaseName + " failed")
    print("Finished in: %s seconds" % (time.time() - start_time))
    reslist.append(time.time() - start_time)
    write_to_csv('res.csv')
