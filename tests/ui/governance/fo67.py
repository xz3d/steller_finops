import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/home/ubuntu/test_pipeline/steller_finops")

import os

try:
    os.chdir("/var/lib/jenkins/workspace/Test/steller_finops/")
    print("Directory changed")
except OSError:
    print("Can't change the Current Working Directory")  

import selenium
import sys
from vulture.ui import *
from vulture.api import *
import time

def main():
    base = Base(sys.argv[1], sys.argv[2], sys.argv[3])
    driver = base.getData()["driver"]

    # Code goes inside this try block
    # DO NOT CODE OUTISDE UNLESS YOU ARE SURE THERE WILL BE NO ERRORS
    # try:
    base.login("testba@ac.com", "Sify#Adm1n")
    base.openFinOpsService()
    base.navigateToMenu(3, "#/governance/overview")
    
    # Required here since charts take a while to load
    time.sleep(10)
    
    base.navigateGraphMenu(0, "Image", "PDF")
    
    time.sleep(5)
    # except:
    #     base.setResult(False)
    #     print("Test failed due to an unexpected error")

    driver.quit()
    return base.getData()["result"]


if __name__ == '__main__':
    testCaseName = os.path.basename(__file__).split('.', 1)[0]
    print("Running " + testCaseName)
    start_time = time.time()
    result = main()
    if result:
        reslist.append(testCaseName)
        reslist.append("PASS")
        print(testCaseName + " passed")
    else:
        reslist.append(testCaseName)
        reslist.append("FAIL")
        print(testCaseName + " failed")
    print("Finished in: %s seconds" % (time.time() - start_time))
    reslist.append(time.time() - start_time)
    write_to_csv('res.csv')
