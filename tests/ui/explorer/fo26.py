import selenium
import sys
import os
import vulture_ui
import time


def main():
    DESCRIBE_TEST_NAME("FO-26")
    DESCRIBE_TEST_SPEC("Instance compare previous and current consumptions")
    base = vulture_ui.Base(sys.argv[1], sys.argv[2], sys.argv[3])
    driver = base.getData()["driver"]

    # Code goes inside this try block
    # DO NOT CODE OUTISDE UNLESS YOU ARE SURE THERE WILL BE NO ERRORS
    try:
        base.login("testba@ac.com", "Sify#Adm1n")
        base.openFinOpsService()
        base.navigateToMenu(2, "#/finops/instances")
        
        prevBill = base.getHeaderInfo(1)[1]
        curBill = base.getHeaderInfo(2)[1]
        timeRangeBill = base.getHeaderInfo(4)[1]
        
        prevBill = base.convertAmountToFloat(prevBill)
        curBill = base.convertAmountToFloat(curBill)
        timeRangeBill = base.convertAmountToFloat(timeRangeBill)
        
        rows = base.getNumberOfRowsInTable(1)
        calculatedTimeRange = 0
        calculatedCur = 0
        
        for i in range(rows):
            calculatedTimeRange += base.convertAmountToFloat(base.getElementFromTable(i,3,False,1).text)
            calculatedCur += base.convertAmountToFloat(base.getElementFromTable(i,4,False,1).text)
        
        if (abs(calculatedTimeRange - timeRangeBill) > 0.1):
            print("Time range consumption not correct")
            base.setResult(False)
        
        if (abs(calculatedCur - curBill) > 0.1):
            print("Current bill consumption not correct")
            base.setResult(False)
    
    except:
        base.setResult(False)
        print("Test failed due to an unexpected error")

    driver.quit()
    return base.getData()["result"]


if __name__ == '__main__':
    testCaseName = os.path.basename(__file__).split('.', 1)[0]
    print("Running " + testCaseName)
    start_time = time.time()
    result = main()
    if result:
        print(testCaseName + " passed")
    else:
        print(testCaseName + " failed")
    print("Finished in: %s seconds" % (time.time() - start_time))
