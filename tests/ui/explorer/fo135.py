import selenium
import sys
import os
import vulture_ui
import time

from vulture import *

def main():
    DESCRIBE_TEST_NAME("FO-135")
    DESCRIBE_TEST_SPEC("UI: Explorer Cost flow graph")
    base = vulture_ui.Base(sys.argv[1], sys.argv[2], sys.argv[3])
    driver = base.getData()["driver"]

    # Code goes inside this try block
    # DO NOT CODE OUTISDE UNLESS YOU ARE SURE THERE WILL BE NO ERRORS
    try:
        base.login("testba@ac.com", "Sify#Adm1n")
        base.openFinOpsService()
        base.navigateToMenu(3, "#/finops/overview")
        
        # Required here since charts take a while to load
        time.sleep(5)
        
        start_time = time.time()
        
        chart_1 = base.getElementByXpath('//div[@id="Domain Consumptions_E"]')
        chart_2 = base.getElementByXpath('//div[@id="3 chartdiv"]')
        chart_3 = base.getElementByXpath('//div[@class="pigraph-barchart"]')
        
        
        if (chart_1 == -1 or chart_2 == -1 or chart_3 == -1):
            base.setResult(False)
            print("Chart not found")
        else:
            print("Loaded charts in: %s seconds" % (time.time() - start_time))

    except:
        base.setResult(False)
        print("Test failed due to an unexpected error")

    driver.quit()
    return base.getData()["result"]


if __name__ == '__main__':
    testCaseName = os.path.basename(__file__).split('.', 1)[0]
    print("Running " + testCaseName)
    start_time = time.time()
    result = main()
    if result:
        reslist.append("PASS")
        print(testCaseName + " passed")
    else:
        reslist.append("FAIL")
        print(testCaseName + " failed")
    write_to_csv('res.csv')
    print("Finished in: %s seconds" % (time.time() - start_time))
