import selenium
import sys
import os
import vulture_ui
import time


def main():
    DESCRIBE_TEST_NAME("FO-28")
    DESCRIBE_TEST_SPEC("To test the edit domain functionality")
    base = vulture_ui.Base(sys.argv[1], sys.argv[2], sys.argv[3])
    driver = base.getData()["driver"]

    # Code goes inside this try block
    # DO NOT CODE OUTISDE UNLESS YOU ARE SURE THERE WILL BE NO ERRORS
    try:
        base.login("testba@ac.com", "Sify#Adm1n")
        base.openFinOpsService()
        base.navigateToMenu(2, "#/finops/instances")
        base.getElementFromTable(1, 6, False, 1).click()
        
        # Required here since charts take a while to load
        time.sleep(10)
        
        chart = base.getElementByXpath('//div[@id="2chartdiv"]')
        
        if (chart == -1):
            base.setResult(False)
            print("Chart not found")

    except:
        base.setResult(False)
        print("Test failed due to an unexpected error")

    driver.quit()
    return base.getData()["result"]


if __name__ == '__main__':
    testCaseName = os.path.basename(__file__).split('.', 1)[0]
    print("Running " + testCaseName)
    start_time = time.time()
    result = main()
    if result:
        print(testCaseName + " passed")
    else:
        print(testCaseName + " failed")
    print("Finished in: %s seconds" % (time.time() - start_time))
