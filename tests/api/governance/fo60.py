import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/home/ubuntu/test_pipeline/steller_finops")

import os

try:
  os.chdir("/home/ubuntu/test_pipeline/steller_finops")
  print("Directory changed")
except OSError:
  print("Can't change the Current Working Directory")  


import requests

from vulture.api import *
DESCRIBE_TEST_NAME("FO-60")
DESCRIBE_TEST_SPEC("Auto Create")
url = DESCRIBE_TEST_URL(API_STELLER_AUTOCREATE)
bearer = steller_api.get_bearer_token() 

headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

result = False

response = requests.request("POST", url, headers=headers, data=PAYLOAD_AUTOCREATE_AWS)
responsejson = json.loads(response.text)

result = aquila_assert(str(response), '<Response [200]>')
result = aquila_assert(responsejson['key'], OPERATION_ADD)
result = aquila_assert(responsejson['variant'], "success")
reslist.append(responsejson['message'])

url = API_STELLER_HIER
response = requests.request("POST", url, headers=headers, data=PAYLOAD_HIER_VIEW)
responsejson = json.loads(response.text)

if len(responsejson) > 1:
    result = False
else:
    result = True

# No two groups should be share\ing a child member

#url = API_STELLER_FETCH
#response = requests.request("POST", url, headers=headers, data=PAYLOAD_FETCH)
#responsejson = json.loads(response.text)
#data_file = 'data_autocreate.json'
#print(aquila_validate_response_with_data(data_file, response.text))

# TODO (Sankrant):
# Add a database query to iterate through list of stray sub domains (unstitched to Company). 
# If exist aafter the fact of api call, the test should fail


# TODO (Sankrant): 
# Validation through cloud consoles

if result == True:
    reslist.append("PASS")
else:
    reslist.append("FAIL")

write_to_csv("res.csv")