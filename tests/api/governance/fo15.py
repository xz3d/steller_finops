import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/home/ubuntu/test_pipeline/steller_finops")

import os

try:
  os.chdir("/home/ubuntu/test_pipeline/steller_finops")
  print("Directory changed")
except OSError:
  print("Can't change the Current Working Directory")  


from vulture.api import *

DESCRIBE_TEST_NAME("FO-15")
url = DESCRIBE_TEST_URL(API_STELLER_EDITTAGS)
VALIDATE_API_IMPLEMENTATION(url)

bearer = steller_api.get_bearer_token() 

headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

response = requests.request("POST", API_STELLER_ADDDOMAIN, headers=headers, data=PAYLOAD_ADD_RESOURCES_ACCOUNTS_DOMAIN)
if (str(response) != "<Response [200]>"):
    print("Error")
    exit(1)
responsejson = ''
if response.text != '':
    responsejson = json.loads(response.text)

result = False

# API soundness
result = aquila_assert(str(response), '<Response [200]>')
result = aquila_assert(responsejson['key'], OPERATION_ADD)
result = aquila_assert(responsejson['variant'], "success")
reslist.append(responsejson['message'])

# NOTE (Sankrant):
# Use the new thread model from manwe

url = API_STELLER_FETCH
response = requests.request("POST", url, headers=headers, data=PAYLOAD_FETCH_DETAILED)
responsejson = ''
print(response.text)
if response.text != '':
    responsejson = json.loads(response.text)
data_file = '/home/ubuntu/test_pipeline/steller_finops/data/data_adddomain.json'
result = aquila_assert(open(data_file, 'r').read(), response.text)

if result == True:
    reslist.append("PASS")
else:
    reslist.append("FAIL")

write_to_csv("res.csv")