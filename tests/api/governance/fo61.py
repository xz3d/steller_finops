import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/home/ubuntu/test_pipeline/steller_finops")

import os

try:
  os.chdir("/home/ubuntu/test_pipeline/steller_finops")
  print("Directory changed")
except OSError:
  print("Can't change the Current Working Directory")  


from vulture.api import *

import math

DESCRIBE_TEST_NAME("FO-61")
DESCRIBE_TEST_SPEC("Compare up down percentage in consumption details")
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

response = requests.request("POST", API_STELLER_SUMMARY, headers=headers, data=PAYLOAD_SUMMARY)
responsejson = json.loads(response.text)

# change in percentage = ((B - A) / A) * 100

result = False

A = float(responsejson[0]['value'])
B = float(responsejson[1]['value'])
C = float(responsejson[2]['value'])
RES1 = ((B - A) / A) * 100 
PER1 = responsejson[1]['percentage']
RES2 = ((C - B) / B) * 100
PER2 = responsejson[2]['percentage']

if math.isclose(PER1, RES1, rel_tol=0.005) and math.isclose(PER2, RES2, rel_tol=0.005):
  result = True
else:
  result = False

response = requests.request("POST", API_STELLER_FETCH, headers=headers, data=PAYLOAD_FETCH_CONSUMPTIONS)
responsejson = json.loads(response.text)

if float(responsejson[0]['currentConsumption']) == B:
  result = True
else:
  result = False

if result == True:
  reslist.append("PASS")
else:
  reslist.append("FAIL")

write_to_csv('res.csv')