import sys
sys.path.append("C:\\t\\Aquila\\steller_finops")

from vulture.api import *

DESCRIBE_TEST_NAME("Add Resource Group")
DESCRIBE_TEST_SPEC("To test the add resource group functionality while adding or editing domains")
url = DESCRIBE_TEST_URL(API_STELLER_ADDDOMAIN)
bearer = steller_api.get_bearer_token() 

headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

response = requests.request("POST", API_STELLER_ADDDOMAIN, headers=headers, data=PAYLOAD_ADD_RESOURCES_GROUP_RESOURCES)
if (str(response) != "<Response [200]>"):
    print("Error")
    exit(1)
responsejson = ''
if response.text != '':
    responsejson = json.loads(response.text)

result = False

# API soundness
result = aquila_assert(str(response), '<Response [200]>')
result = aquila_assert(responsejson['key'], OPERATION_ADD)
result = aquila_assert(responsejson['variant'], "success")
reslist.append(responsejson['message'])


url = API_STELLER_HIER
response = requests.request("POST", url, headers=headers, data=PAYLOAD_HIER_VIEW)
responsejson = json.loads(response.text)

if result == True:
    reslist.append("PASS")
else:
    reslist.append("FAIL")

write_to_csv("res.csv")