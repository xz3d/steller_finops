import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/home/ubuntu/test_pipeline/steller_finops")

import os

try:
  os.chdir("/home/ubuntu/test_pipeline/steller_finops")
  print("Directory changed")
except OSError:
  print("Can't change the Current Working Directory")  


from vulture.api import *

reslist.append("FO-11")
reslist.append("Edit Service Domain")
url = API_STELLER_EDITSERVICEDOMAIN
VALIDATE_API_IMPLEMENTATION(url)
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=PAYLOAD_FETCH_DETAILED)
responsejson = json.loads(response.text)

result = False
result = aquila_assert(str(response), '<Response [200]>')
