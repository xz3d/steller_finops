import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/home/ubuntu/test_pipeline/steller_finops")

import os

try:
  os.chdir("/home/ubuntu/test_pipeline/steller_finops")
  print("Directory changed")
except OSError:
  print("Can't change the Current Working Directory")  

from vulture.api import *

import math

DESCRIBE_TEST_NAME("FO-62")
DESCRIBE_TEST_SPEC("To test that company sub domain details are listed")
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

'''
response = requests.request("POST", API_STELLER_FETCH, headers=headers, data=PAYLOAD_FETCH_DETAILED2)
responsejson = json.loads(response.text)
result = aquila_assert(str(response), '<Response [200]>')

print(response.text)
# TODO (Sankrant): Aquila Assert()

response = requests.request("POST", API_STELLER_SUMMARY, headers=headers, data=PAYLOAD_SUMMARY_DETAILED)
responsejson = json.loads(response.text)
result = aquila_assert(str(response), '<Response [200]>')

print(response.text)
# TODO (Sanrant): Aquila Assert()
'''

expected_scheme = '[{"percentage":"","progressTitle":"","amtTitle":"Total Budget","amount":"$ 100","amtSize":""},{"percentage":"","progressTitle":"","amtTitle":"Time Range","amount":"2021-02-01 to 2021-03-31 (Reset Monthly)","amtSize":"small"}]'
response = requests.request("POST", API_STELLER_EXTENDED_SUMMARY, headers=headers, data=PAYLOAD_EXTENDED_SUMMARY_DETAILED)
responsejson = json.loads(response.text)

result = aquila_assert(str(response), '<Response [200]>')
if (response.text == expected_scheme):
  result = True
else:
  result = False
  
if result:
  reslist.append("PASS")
else:
  reslist.append("FAIL")

write_to_csv('res.csv')