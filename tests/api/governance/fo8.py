import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/home/ubuntu/test_pipeline/steller_finops")

import os

try:
  os.chdir("/home/ubuntu/test_pipeline/steller_finops")
  print("Directory changed")
except OSError:
  print("Can't change the Current Working Directory")  


from vulture.api import *

DESCRIBE_TEST_NAME("FO-8")
DESCRIBE_TEST_SPEC("To test the edit domain functionality")
url = DESCRIBE_TEST_URL(API_STELLER_EDITDOMAIN)
bearer = steller_api.get_bearer_token() 

headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

result = False

response = requests.request("POST", url, headers=headers, data=PAYLOAD_EDIT_SIFYCSV)
result = aquila_assert(str(response), '<Response [200]>')
if result == False:
  reslist.append("FAIL")
  write_to_csv("res.csv")
  exit(1)
responsejson = json.loads(response.text)

# API soundness
result = aquila_assert(responsejson['key'], OPERATION_EDIT)
result = aquila_assert(responsejson['variant'], "success")
reslist.append(responsejson['message'])


# NOTE (Sankrant):
# Use the new thread model from manwe

url = API_STELLER_HIER
response = requests.request("POST", url, headers=headers, data=PAYLOAD_HIER_VIEW)
responsejson = json.loads(response.text)
data_file = 'data_editdomain.json'
result = aquila_validate_response_with_data(data_file, response.text)

if result == True:
    reslist.append("PASS")
else:
    reslist.append("FAIL")

write_to_csv("res.csv")