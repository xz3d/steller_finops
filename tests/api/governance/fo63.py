import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/home/ubuntu/test_pipeline/steller_finops")

import os

try:
  os.chdir("/home/ubuntu/test_pipeline/steller_finops")
  print("Directory changed")
except OSError:
  print("Can't change the Current Working Directory")  


from vulture.api import *

import math

DESCRIBE_TEST_NAME("FO-63")
DESCRIBE_TEST_SPEC("Company Sub Domain Details")
url = DESCRIBE_TEST_URL(API_STELLER_FETCH)
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=PAYLOAD_FETCH_BUDGET)
responsejson = json.loads(response.text)

X = responsejson[0]["BUDGET"]

response = requests.request("POST", url, headers=headers, data=PAYLOAD_FETCH_BUDGET_DETAILED)
responsejson = json.loads(response.text)

Y = responsejson[0]["BUDGET"]

result = aquila_assert(X, Y)


if result == True:
    reslist.append("PASS")
else:
    reslist.append("FAIL")

write_to_csv('res.csv')
