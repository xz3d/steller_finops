# TODO (SANKRANT): PLEASE DONT RUN THIS TEST
# NOTE (SANKRANT): PLEASE DONT RUN THIS TEST

exit(42)
from vulture.api import *


try:
  os.chdir("/home/ubuntu/test_pipeline/steller_finops")
  print("Directory changed")
except OSError:
  print("Can't change the Current Working Directory")  


DESCRIBE_TEST_NAME("FO-6")
DESCRIBE_TEST_SPEC("Test Delete Domain Feature")
url = DESCRIBE_TEST_URL(API_STELLER_DEL)
VALIDATE_API_IMPLEMENTATION(url)

bearer = steller_api.get_bearer_token() 

headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

#response = requests.request("POST", API_STELLER_DEL, headers=headers, data=PAYLOAD_DELETE_AQUILA_MARKETING)

id_ = int(open('groupid.k', 'r').read())
print(id_)
response = requests.request("POST", API_STELLER_DEL, headers=headers, data=payload_delete_group_id(304))
responsejson = json.loads(response.text)

result = False

# API soundness
result = aquila_assert(str(response), '<Response [200]>')
result = aquila_assert(responsejson['key'], OPERATION_DELETE)
result = aquila_assert(responsejson['variant'], "success")
reslist.append(responsejson['message'])

if result == False:
  exit(1)

# NOTE (Sankrant):
# Use the new thread model from manwe

#url = API_STELLER_FETCH
#response = requests.request("POST", url, headers=headers, data=PAYLOAD_FETCH_DELETE)
#responsejson = json.loads(response.text)
#data_file = 'data_adddomain.json'
#result = aquila_assert(open(data_file, 'r').read(), response.text)

url = API_STELLER_HIER
response = requests.request("POST", url, headers=headers, data=PAYLOAD_HIER_VIEW)
responsejson = json.loads(response.text)


for i in responsejson[0]['children']:
  if i['groupName'] == "DemonstrationByZed":
    result = False
    break
  else:
    result = True

if result == True:
    reslist.append("PASS")
else:
    reslist.append("FAIL")

write_to_csv("res.csv")