import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/home/ubuntu/test_pipeline/steller_finops")

import os

try:
  os.chdir("/home/ubuntu/test_pipeline/steller_finops")
  print("Directory changed")
except OSError:
  print("Can't change the Current Working Directory")  


from vulture.api import *

DESCRIBE_TEST_NAME("FO-16")
DESCRIBE_TEST_SPEC("To test the add resources functionality while adding or editing domains")
url = DESCRIBE_TEST_URL(API_STELLER_ADDDOMAIN)
bearer = steller_api.get_bearer_token() 

headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

response = requests.request("POST", API_STELLER_ADDDOMAIN, headers=headers, data=PAYLOAD_ADD_SERVICE_DOMAIN)
if (str(response) != "<Response [200]>"):
    print("Error")
    exit(1)
responsejson = ''
if response.text != '':
    responsejson = json.loads(response.text)

result = False

# API soundness
result = aquila_assert(str(response), '<Response [200]>')
result = aquila_assert(responsejson['key'], OPERATION_ADD)
result = aquila_assert(responsejson['variant'], "success")
reslist.append(responsejson['message'])

url = API_STELLER_HIER
response = requests.request("POST", url, headers=headers, data=PAYLOAD_HIER_VIEW)
responsejson = json.loads(response.text)

for i in responsejson[0]['children']:
  if i['groupName'] == "DemonstrationByZed":
    f = open('groupid.k', 'w+')
    if (i['groupId'] == i['key']):
      f.write(str(i['key']))
      f.close()
    result = True
    break
  else:
    result = False


if result == True:
    reslist.append("PASS")
    write_to_csv("res.csv")
else:
    reslist.append("FAIL")
    write_to_csv("res.csv")
    exit(1)