import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/var/lib/jenkins/workspace/Test/steller_finops")

from vulture.api import *

import math

DESCRIBE_TEST_NAME("FO-77")
DESCRIBE_TEST_SPEC("Scaling Group Fetch")
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

result = False

response = requests.request("POST", API_STELLER_SCALING_GROUP_FETCH, headers=headers, data=PAYLOAD_SCALING_GROUP_FETCH)
if response.text == '[]':
  reslist.append("FAIL")
  write_to_csv('res.csv')
  exit(1)

responsejson = json.loads(response.text)

result = aquila_assert(str(response), '<Response [200]>')

A = float(responsejson[0]['value'])
B = float(responsejson[1]['value'])
C = float(responsejson[2]['value'])
RES1 = ((B - A) / A) * 100 
PER1 = responsejson[1]['percentage']
RES2 = ((C - B) / B) * 100
PER2 = responsejson[2]['percentage']

if math.isclose(PER1, RES1, rel_tol=0.005) and math.isclose(PER2, RES2, rel_tol=0.005):
  result = True
else:
  result = False
if result:
  reslist.append("PASS")
else:
  reslist.append("FAIL")

write_to_csv('res.csv')