import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/var/lib/jenkins/workspace/Test/steller_finops")


from vulture.api import *

import math

DESCRIBE_TEST_NAME("FO-22")
DESCRIBE_TEST_SPEC("Compare up down percentage in consumption details")
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

response = requests.request("POST", API_STELLER_SUMMARY, headers=headers, data=PAYLOAD_SUMMARY)
responsejson = json.loads(response.text)

# change in percentage = ((B - A) / A) * 100

result = False

A = float(responsejson[0]['value'])
B = float(responsejson[1]['value'])
C = float(responsejson[2]['value'])
RES1 = ((B - A) / A) * 100 
PER1 = responsejson[1]['percentage']
RES2 = ((C - B) / B) * 100
PER2 = responsejson[2]['percentage']

if math.isclose(PER1, RES1, rel_tol=0.005) and math.isclose(PER2, RES2, rel_tol=0.005):
  result = True
else:
  result = False

if result:
  reslist.append("PASS")
else:
  reslist.append("FAIL")

write_to_csv('res.csv')