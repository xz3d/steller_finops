import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/var/lib/jenkins/workspace/Test/steller_finops")

from vulture.api import *

import math

DESCRIBE_TEST_NAME("FO-80")
DESCRIBE_TEST_SPEC("Individual")
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

result = False

response = requests.request("POST", API_STELLER_TIMESERIES_COST_SG, headers=headers, data=PAYLOAD_TIMESERIES_INSTANCE_COUNT_SG)
if response.text == '[]':
  reslist.append("FAIL")
  write_to_csv('res.csv')
  exit(1)
responsejson = json.loads(response.text)
result = aquila_assert(str(response), '<Response [200]>')

response = requests.request("POST", API_STELLER_TIMESERIES_COUNT_SG, headers=headers, data=PAYLOAD_TIMESERIES_UTILIZATION_COUNT_SG)
if response.text == '[]':
  reslist.append("FAIL")
  write_to_csv('res.csv')
  exit(1)
responsejson = json.loads(response.text)
result = aquila_assert(str(response), '<Response [200]>')

if result:
  reslist.append("PASS")
else:
  reslist.append("FAIL")

write_to_csv('res.csv')