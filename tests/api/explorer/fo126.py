import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/var/lib/jenkins/workspace/Test/steller_finops")

from vulture.api import *

import math

DESCRIBE_TEST_NAME("FO-126")
DESCRIBE_TEST_SPEC("Services Fetch")
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

result = False

response = requests.request("POST", API_STELLER_SERVICES_FETCH, headers=headers, data=PAYLOAD_SERVICES_FETCH)
if response.text == '[]':
  reslist.append("FAIL")
  write_to_csv('res.csv')
  exit(1)

responsejson = json.loads(response.text)

result = aquila_assert(str(response), '<Response [200]>')

value = responsejson[0]['value']

aws_validate()

if result:
  reslist.append("PASS")
else:
  reslist.append("FAIL")

write_to_csv('res.csv')