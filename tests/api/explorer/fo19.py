import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/var/lib/jenkins/workspace/Test/steller_finops")

from vulture.api import * 

DESCRIBE_TEST_NAME("FO-19")
DESCRIBE_TEST_SPEC("Explorer domain details")
url = DESCRIBE_TEST_URL(API_STELLER_EXPLORER_SUMMARY)

bearer = steller_api.get_bearer_token() 

result = False

headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=PAYLOAD_EXPLORER_SUMMARY_MAIN)

if str(response) == '<Response [200]>':
    result = False
    write_to_csv('res.csv')

responsejson = json.loads(response.text)

value = float(responsejson[0]['value'])

response = requests.request("POST", API_STELLER_EXPLORER_FETCH_SUMMARY_DETAILED, headers=headers, data=PAYLOAD_EXPLORER_FETCH_DETAILED)
responsejson = json.loads(response.text)

budgetlist = []

for i in range(len(responsejson)):
    budgetlist.append(dict(groupName = str(responsejson[i]['groupName']),  budget = float(responsejson[i]['BUDGET'])))

if budgetlist != []:
    result = True
else:
    result = False

if result:
    reslist.append("PASS")
else:
    reslist.append("FAIL")

write_to_csv('res.csv')