import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/var/lib/jenkins/workspace/Test/steller_finops")

from vulture.api import *

import math

DESCRIBE_TEST_NAME("FO-4")
DESCRIBE_TEST_SPEC("Storage Costs Previous Consumptions")
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

result = False

response = requests.request("POST", API_STELLER_FETCH_INSTANCE, headers=headers, data=PAYLOAD_FETCH_INSTANCE)
responsejson = json.loads(response.text)

result = aquila_assert(str(response), '<Response [200]>')

value = responsejson[0]['value']

if value < 0:
    result = False
    reslist.append("FAIL")
    write_to_csv('res.csv')
    exit(-1)

aws_validate()

if result:
  reslist.append("PASS")
else:
  reslist.append("FAIL")

write_to_csv('res.csv')