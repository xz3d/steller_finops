import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/var/lib/jenkins/workspace/Test/steller_finops")


from vulture.api import *

import math

DESCRIBE_TEST_NAME("FO-24")
DESCRIBE_TEST_SPEC("Domain dropdown details")
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

result = False

response = requests.request("POST", API_STELLER_FETCH, headers=headers, data=PAYLOAD_EXPLORER_FETCH)
responsejson = json.loads(response.text)

value = float(responsejson[0]['currentConsumption'])

response = requests.request("POST", API_STELLER_RESOURCE_TYPE, headers=headers, data=PAYLOAD_EXPLORER_RESOURCE_TYPE)
responsejson = json.loads(response.text)

sum = 0
for i in range(len(responsejson)):
    sum = sum + float(responsejson[i]['cost'])

if sum == value:
    result = True
else:
    result = False

if result:
  reslist.append("PASS")
else:
  reslist.append("FAIL")

write_to_csv('res.csv')