import sys
if sys.platform == 'win32':
    sys.path.append("C:\\t\\Aquila\\steller_finops")
else:
    sys.path.append("/var/lib/jenkins/workspace/Test/steller_finops")


from vulture.api import *

import math

DESCRIBE_TEST_NAME("FO-148")
DESCRIBE_TEST_SPEC("Reservation instance details")
bearer = steller_api.get_bearer_token() 
headers = {
  'Authorization': 'Bearer ' + str(bearer),
  'Content-Type': 'application/json'
}

response = requests.request("POST", API_STELLER_RESERVATION_SUMMARY, headers=headers, data=PAYLOAD_RESERVATION_SUMMARY)
responsejson = json.loads(response.text)

# change in percentage = ((B - A) / A) * 100
result = aquila_assert(str(response), '<Response [200]>')

A = float(responsejson[0]['value'])
B = float(responsejson[1]['value'])
C = float(responsejson[2]['value'])

if result:
  reslist.append("PASS")
else:
  reslist.append("FAIL")

write_to_csv('res.csv')